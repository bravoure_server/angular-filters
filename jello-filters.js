(function () {

    'use strict';

    // Removes the empty spaces at the end of the file
    function truncate () {

        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt (max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr (0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf (' ');
                if (lastspace != -1) {
                    //Also remove . and , so its gives a cleaner result.
                    if (value.charAt (lastspace - 1) == '.' || value.charAt (lastspace - 1) == ',') {
                        lastspace = lastspace - 1;
                    }
                    value = value.substr (0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    }

    // Capitalize a text
    function capitalize() {
        return function(input) {
            return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
        }
    }

    angular
        .module ('bravoureAngularApp')
        .filter('capitalize', capitalize)
        .filter ('truncate', truncate);

}) ();
